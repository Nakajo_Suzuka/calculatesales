package jp.alhinc.nakajo_suzuka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();  //支店定義ファイル
		Map<String, Long> branchSales = new HashMap<>();   //売上ファイル
		
		Map<String, String> commodityNames = new HashMap<>();  //商品定義ファイル
		Map<String, Long> commoditySales = new HashMap<>();  //商品別集計ファイル
		
		//コマンド引数が渡されているかどうか
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		if (!inputFile(args[0], "branch.lst", branchNames, branchSales, "支店", "^\\d{3}")) {
			return;
		}
		if (!inputFile(args[0], "commodity.lst", commodityNames, commoditySales, "商品", "^[0-9a-zA-Z]{8}$")) {
			return;
		}
		
		//売上ファイル読込
		BufferedReader br = null;
		
		File [] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		
		//8桁の数字かつ.rcdファイルを抽出
		for (int i = 0; i < files.length ; i++) {
			if (files[i].isFile() && files[i].getName().matches("^\\d{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		
		//売上ファイルのソート
		Collections.sort(rcdFiles);
				
		//売上ファイル名が連番になっていません
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				File rcdFile = rcdFiles.get(i);
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);
				
				String line;
				ArrayList<String> rcdFilesSale = new ArrayList<String>();
				while ((line = br.readLine()) != null) {
					rcdFilesSale.add(line);
				}
				
				//＜該当ファイル名＞のフォーマットが不正です
				if (rcdFilesSale.size() != 3) {
					System.out.println(rcdFile.getName() + "のフォーマットが不正です");
					return;
				}
				
				//売上金額が数字かどうか
				if (!rcdFilesSale.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				
				//<該当ファイル名>の支店コードが不正です
				if (!branchNames.containsKey(rcdFilesSale.get(0))) {
					System.out.println(rcdFile.getName() + "の支店コードが不正です");
					return;
				}
				
				//<該当ファイル名>の商品コードが不正です
				if (!commodityNames.containsKey(rcdFilesSale.get(1))) {
					System.out.println(rcdFile.getName() + "の商品コードが不正です");
					return;
				}
				
				long fileSale = Long.parseLong(rcdFilesSale.get(2));
				Long saleAmount = branchSales.get(rcdFilesSale.get(0)) + fileSale;	
				Long saleCommodity = commoditySales.get(rcdFilesSale.get(1)) + fileSale;
				
				//支店別の売上合計金額が10桁を超えました
				if (saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//商品別の売上合計金額が10桁を超えました
				if (saleCommodity >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				branchSales.put(rcdFilesSale.get(0), saleAmount); //支店コード，売上金額
				commoditySales.put(rcdFilesSale.get(1), saleCommodity); //商品コード，売上金額
				
				
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br!= null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if (!outputFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if (!outputFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}
	
	//支店・商品定義ファイル入力
	private static boolean inputFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String type, String range) {
		BufferedReader br = null;
		
		try {
			File file = new File(path, fileName);
			
			//支店・商品定義ファイルが存在しません
			if (!file.exists()) {
				System.out.println(type + "定義ファイルが存在しません");
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				
				//支店・商品定義ファイルのフォーマットが不正です
				if ((items.length != 2) || (!items[0].matches(range))) {
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}
				
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br!= null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	
	//ファイル出力
	private static boolean outputFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		
		try {
			File file = new File(path, fileName); 
			FileWriter wr = new FileWriter(file);
			bw = new BufferedWriter(wr);
			
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw!= null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}